package org.nrg.xnat.generated.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "radiologics_plugin_usertabs", name = "XNAT 1.7 Radiologics Usertabs Plugin", description = "This is the XNAT 1.7 Radiologics Usertabs Plugin.")
public class RadiologicsUsertabsPlugin {
}